<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */
Route::get('/', function () {
   return view("home",["errs"=>null]);
});

Route::group([
    'prefix' => 'loans',
    'middleware' => ['web']
], function () {
    Route::post('/', 'Loans\AccountController@apply');
    Route::get('/', 'Loans\AccountController@result');
    Route::get('/search', 'Loans\AccountController@search');
    Route::post('/search', 'Loans\AccountController@retrieve');
});
