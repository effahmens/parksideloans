CREATE DATABASE `parksidelending` /*!40100 DEFAULT CHARACTER SET utf8 */;

use parksidelending;

CREATE TABLE `loanaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL DEFAULT '0',
  `propertyvalue` double NOT NULL DEFAULT '0',
  `ssn` char(12) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `loanid` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loanid_UNIQUE` (`loanid`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

CREATE DATABASE `parksidelendingtest` /*!40100 DEFAULT CHARACTER SET utf8 */;

use parksidelendingtest;

CREATE TABLE `loanaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL DEFAULT '0',
  `propertyvalue` double NOT NULL DEFAULT '0',
  `ssn` char(12) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `loanid` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loanid_UNIQUE` (`loanid`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
