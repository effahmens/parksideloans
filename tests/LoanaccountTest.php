<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Loanaccount;

class LoanaccountTest extends TestCase
{
	private $account;
	/**
	*@before
	*/
    public function setup(){
    	$this->account = new Loanaccount;
    } 

    public function testRejectedLoan()
    { 
    	$this->account->__construct1(40, 60, "123-45-6789");
        $this->assertEquals(Loanaccount::REJECTED, $this->account->getStatus(),"Loan is rejected"); 
    }

    public function testAcceptedLoan()
    {  
        $this->account->__construct1(12, 60, "123-45-6789");
        $this->assertEquals(Loanaccount::ACCEPTED, $this->account->getStatus(),"Loan is accepted");
    }

    public function teststatusTestLoan()
    {  
        $this->account->__construct1(12, 60, "123-45-6789");
        $this->assertEquals("Accepted", $this->account->getStatusText(),"StatusText test");
    }

    public function testCalculateLTV(){
        $this->account->__construct1(50, 60, "123-45-6789"); 
        $this->assertGreaterThan(40, $this->account->calculateLTV(),"Calculate LTV");
    }
}
