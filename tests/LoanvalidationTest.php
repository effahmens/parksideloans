<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Loanvalidation;

class LoanvalidationTest extends TestCase
{
     public function testValidationSSN(){
     	$this->assertEquals("123-45-6789", Loanvalidation::formatSSN("123456789"), "Testing formating of SSN");
     }

     public function testInputValidation(){
     	$data = array(
     		"amount"=>40,
     		"propertyvalue"=>20,
     		"ssn" =>"123-45-6789"
     	);
     	$this->assertEquals($data, Loanvalidation::canCreateLoan($data), "Input Validation Test");
     }

     public function testStripspaces(){
     	$this->assertEquals("123456", Loanvalidation::stripspaces("1 2 3 4 5 6 "), "Test remove white spaces");
     }
}
