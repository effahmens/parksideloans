<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Loanfactory;

class LoanfactoryTest extends TestCase
{
     public function testCreateLoanAccepted(){
     	$result = Loanfactory::createLoan(12,90,"123456789");
     	$this->assertEquals("SUCCESS", $result["status"],"Loan is accepted and created");
     }

     public function testCreateLoanRejected(){
     	$result = Loanfactory::createLoan(120,90,"123456789");
     	$this->assertEquals("ERROR", $result["status"],"Loan is rejected");
     }

     public function testfindByLoanId(){
     	$result = Loanfactory::createLoan(12,90,"123456789");
     	$loan = Loanfactory::findByLoanId($result['data']->getLoanId());
     	$this->assertEquals($result['data']->getLoanId(), $loan->getLoanId(),"Find loan using loanid");
     	$this->assertEquals(12, $loan->getAmount(),"Loan Amount");
     }
}
