<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LoanToValue;
use Webpatser\Uuid\Uuid;

class Loanaccount extends Model
{
    use LoanToValue;

    protected $fillable =[
        'loanid',
        'amount',
        'propertyvalue',
        'ssn',
        'status',
    ];

    protected $hidden =[
        'id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    const ACCEPTED = 1, REJECTED = 2;
    public function __construct1($amount, $propertyvalue, $ssn)
    {
        parent::__construct();
        $this->setAmount($amount);
        $this->setPropertyValue($propertyvalue);
        $this->setSSN($ssn);
        $this->setStatus();
    }

    public function __construct(){}

    public function getLoanId()
    {
        return $this->loanid;
    }

    public function getId()
    {
        return $this->id;
    }

    private function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    private function setPropertyValue($propertyvalue)
    {
        $this->propertyvalue = $propertyvalue;
    }

    public function getPropertyValue()
    {
        return $this->propertyvalue;
    }

    private function setSSN($ssn)
    {
        $this->ssn = $ssn;
    }

    public function getSSN()
    {
        return $this->ssn;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusText(){
      return ($this->getStatus() === Loanaccount::ACCEPTED)?"Accepted":"Rejected";
    }

    private function setStatus()
    {
        if ($this->calculateLTV() > 40) {
            $this->status = Loanaccount::REJECTED;
        } else {
            $this->status = Loanaccount::ACCEPTED;
        }
    }
}