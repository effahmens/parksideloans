<?php
namespace App\Models;

use App\Models\Loanaccount;
use App\Models\Loanvalidation;
use Webpatser\Uuid\Uuid;

class Loanfactory
{ 
    public static function createLoan($amount, $propertyvalue, $ssn)
    {  
        //validate loan data to be inserted into database;
        $validated = Loanvalidation::canCreateLoan(array(
            "amount" => Loanvalidation::stripspaces($amount),
            "propertyvalue" => Loanvalidation::stripspaces($propertyvalue),
            "ssn" => Loanvalidation::formatSSN($ssn)
        ));

        if (!is_array($validated)){
            return array(
                "data"=> $validated,
                "status" =>"ERROR"
            );
        } 
        
        $account = new Loanaccount;  
        $account->__construct1($validated['amount'], $validated['propertyvalue'], $validated['ssn']);

        if($account->getStatus() === Loanaccount::REJECTED){
            return array(
                "data"=> "We cannot accept the loan.",
                "status" =>"ERROR"
            );
        }

        $account->save(array( 
            "amount" => $account->getAmount(),
            "propertyvalue" => $account->getPropertyValue(),
            "ssn" => $account->getSSN(),
            "status" => $account->getStatus()
        ));
        
        return array(
            "data"=> Loanfactory::updateLoanId($account),
            "status" =>"SUCCESS"
        );
    }

    private static function updateLoanId(Loanaccount $account){
       $account = Loanfactory::findById($account->getId()); 
       if($account->getLoanId() == NULL || empty($account->getLoanId())){ 
          $account->update(["loanid"=> Loanfactory::generateLoanId($account->getId())]);
       }
       return $account;
    }

    private static function generateid($id){
        return substr(Uuid::generate(5, $id.time(), 
            Uuid::NS_DNS)->string,0,12);
    }

    private static function generateLoanId($id){
        $lid = Loanfactory::generateid($id);
        $loan = Loanfactory::findByLoanId( $lid);
        if($loan){
            return Loanfactory::generateLoanId($id);
        }
        return  $lid;
    }

    public static function findById($id)
    { 
        if($id===NULL)
            return NULL;

        return Loanaccount::where("id",$id)->first(); 
    }

    public static function findByLoanId($loanid)
    { 
        if($loanid===NULL)
            return NULL;

        return Loanaccount::where("loanid", $loanid)->first(); 
    }

    public static function findLoans(){
        return Loanaccount::all();
    }
}