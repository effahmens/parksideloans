<?php
namespace App\Models;

require_once base_path('vendor/wixel/gump/gump.class.php');

class Loanvalidation
{

    public static function canCreateLoan($data)
    {
        $rules = array(
            'ssn' => 'regex,/^(\d{3})-(\d{2})-(\d{4})$/',
            'propertyvalue' => 'required|numeric|min_numeric,1',
            'amount' => 'required|numeric|min_numeric,1'
        );
        return Loanvalidation::validate($data, $rules);
    }

    public static function validate($data, $rules)
    {
        $gump = new \GUMP();
        $data = $gump->sanitize($data);
        $gump->validation_rules($rules);
        $validated_data = $gump->run($data);
        if ($validated_data === false) {
            return $gump->get_readable_errors(true);
        } else {
            return $validated_data;
        }
    } 

    public static function formatSSN($ssn){
        $ssn = Loanvalidation::stripspaces($ssn);
        $ssn = Loanvalidation::numeric_only($ssn);
        return preg_replace("/^(\d{3})(\d{2})(\d{4})$/", "$1-$2-$3", $ssn);
    }

    public static function stripspaces($string){
        return preg_replace('/\s+/', '', $string);
    }

    public static function numeric_only($string){
        return preg_replace("/[^0-9]/", "", $string);
    }

    public static function last4SSN($ssn){
        return substr($ssn, strlen($ssn)-4, 4);
    }
}