<?php
namespace App\Models;

trait LoanToValue{

    public function calculateLTV()
    { 
    	if($this->getPropertyValue()<=0)
    		return 0;
    	
        return (($this->getAmount() / $this->getPropertyValue()) * 100);
    }

    abstract public function getAmount();

    abstract public function getPropertyValue();
}