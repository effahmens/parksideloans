<?php
namespace App\Http\Controllers\Loans;

use App\Http\Controllers\Controller;
use App\Models\Loanfactory;
use App\Models\Loanvalidation;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;

class AccountController extends Controller
{ 
    public function apply(Request $request)
    { 
        $result = Loanfactory::createLoan(
             $request->input('amount'), 
             $request->input('propertyvalue'), 
             $request->input('ssn')
        );

        if($result['status'] === "ERROR"){ 
            return view("home", ["errs" =>$result['data'] ]);
        }else{ 
            return view("result", ["account"=>$this->transform($result['data'])] );
        }
    }

    public function result(Request $request){
        return view("result",["account" => null]);
    }

    public function search(Request $request)
    {
		return view("search");
    }

    public function retrieve(Request $request){  
        $result = Loanfactory::findByLoanId($request->input('loanid'));
        $data = NULL;
        if($result){
            $data =$this->transform($result);
        } 
        return view("result", ["account" => $data]);
    }

    private function transform($result){
        return array(
            "loanid" => $result->getLoanid(), 
            "status" => $result->getStatusText()
        );
    }
}