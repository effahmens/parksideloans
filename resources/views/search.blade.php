@extends('layouts.main')

@section('content')
	<div class="container">
        <h2>Check Your Loan Status</h2>
		<form id="search_loan" action="{{url('loans/search')}}" class="search_form" method="post">
	          {{ csrf_field() }} 
	            <div class="input-group">
	              <span class="input-group-addon first" id="basic-addon3">LOAN ID</span>
	              <input type="text" class="form-control" name="loanid" id="loanid" placeholder="">
	              <span class="input-group-addon"></span>
	            </div>
	            <button id="btn_search_loan" type="submit" class="btn btn-success">Check Status</button>
	    </form>
    </div>
@endsection