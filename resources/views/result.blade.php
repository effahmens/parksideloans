@extends('layouts.main')

@section('content')
	<div class="container">
		<h2>Search Result</h2>
		@if($account)
		 <div  class="result">
		 	<p class="rtitle">Loan ID</p>
		 	<p class="rvalue">{{$account['loanid']}}</p>
		 </div>

		 <div class="result">
		 	<p class="rtitle">Loan Status</p>
		 	<p class="rvalue">{{$account['status']}}</p>
		 </div>
		 @else
		 	<div>
		 		<h4>No Loan Found</h4>
		 	</div>
		 @endif
	</div>
@endsection