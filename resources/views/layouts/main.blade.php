<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>ParksideLending Loan Portal</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet"> 
        @yield('css')
    </head>
    <body>
        <div class="container">
             
            <div class="content">
                <div class="title m-b-md">
                   ParksideLending Loan Portal
                </div>

                 @yield('content')

                <div class="links">
                    <a class="btn btn-primary" href="{{url('/')}}">Create New Loan</a>
                    <a  class="btn btn-primary" href="{{url('loans/search')}}">Check Loan Status</a>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script> 
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script> 
        <script src="{{ asset('js/main.js') }}"></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        @yield('js')
    </body>
</html>