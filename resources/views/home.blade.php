@extends('layouts.main')

@section('content')
    <div class="container">
        <h3>Need A Loan? Apply Now!</h3>
        <form id="create_loan" action="{{url('loans')}}" class="loan_form" method="post">
          {{ csrf_field() }}
           <label class="basic-label" for="amount">How much loan do you need?</label>
            <div class="input-group">
              <span class="input-group-addon first">Loan Amount ($)</span>
              <input type="number" id="amount" name="amount" class="form-control" aria-label="Amount (to the nearest dollar)" >
              <span class="input-group-addon">.00</span>
            </div>

            <label class="basic-label" for="property">What's the value of your property?</label>
            <div class="input-group">
              <span class="input-group-addon first">Property Value ($)</span>
              <input type="number" class="form-control" aria-label="Amount (to the nearest dollar)" id="propertyvalue" name="propertyvalue" >
              <span class="input-group-addon">.00</span>
            </div>

            <label class="basic-label" for="ssn_number">Enter your 9 digit Social Security</label>
            <div class="input-group">
              <span class="input-group-addon first" id="basic-addon3">SSN</span>
              <input type="text" class="form-control" name="ssn" id="ssn" placeholder="###-##-####" maxlength="12"  >
              <span class="input-group-addon">  </span>
            </div>
           
              @if($errs)
                <div class="alert alert-danger" role="alert"">
                  {{!! $errs !!}}
                </div>
              @endif
           
            <button id="btn_apply_loan" type="submit" class="btn btn-success">
            Apply For Loan</button>
        </form>
    </div>
@endsection