$("#create_loan").validate({
  rules: {
    amount:{
      required: true,
  	  number: true
    },
    propertyvalue:{
      required: true,
  	  number: true
    },
    ssn:{
     required:true,
     maxlength:12
    }
  },submitHandler: function(form) {
  	  form.submit();
  }
});