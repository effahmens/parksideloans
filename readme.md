#Code Submission by Effah Philips.

##Description
This application was built using PHP Laravel framework using composer for library management.

##Code Location
All code by me for this application is located in these folders
	1. parksideloans/app/models
	2. parksideloans/app/Http/Controllers/Loans
	3. parksideloans/test
	4. parksideloans/resources/view
	5. parksideloans/routes
	6. parksideloans/public/js and css

##Http Server Requirement
Apache Server, Ngnix

##Database 
MySQL

#Minimum PHP Version
5.6.4

##Instructions
1. Clone the application into the root directory of the Http server where the web files
reside.
2. Run composer update [Install composer if not installed]
3. With Laravel you have to point the root directory of the Http server to public.
   Eg. if the application folder is parksideloans. You can get access to the running application
   from parksideloans/public so if running in production you have to point the application base or home
   to parksideloans/public.
4. Run the database script "db.sql" in the root directory to create the database and table
5. The database name is "parksidelending" and table is "loanaccounts"
### ENV file
	a. Create an .env file with the following content
	```
		APP_ENV=local
		APP_KEY=base64:ufRdRZtJ6aI0cBKkc1dls3ZN8Tw5Y/NcsE8vasAMArw=
		APP_DEBUG=true
		APP_LOG_LEVEL=debug
		APP_URL=http://localhost

		DB_CONNECTION=mysql
		DB_HOST=127.0.0.1
		DB_PORT=3306
		DB_DATABASE=parksidelending
		DB_USERNAME=root
		DB_PASSWORD=root

		BROADCAST_DRIVER=log
		CACHE_DRIVER=file
		SESSION_DRIVER=file
		QUEUE_DRIVER=sync

		REDIS_HOST=127.0.0.1
		REDIS_PASSWORD=null
		REDIS_PORT=6379

		MAIL_DRIVER=smtp
		MAIL_HOST=mailtrap.io
		MAIL_PORT=2525
		MAIL_USERNAME=null
		MAIL_PASSWORD=null
		MAIL_ENCRYPTION=null

		PUSHER_APP_ID=
		PUSHER_KEY=
		PUSHER_SECRET=
	```

	b. Change the database password and username in the .env file to match your own.

#PHPUnit Test
Test files can be found in the folder test. 
Install phpunit global package using composer and set composer/bin/ to environment
NB: To run test, cd into the application root directory
```
run phpunit
```